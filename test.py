import unittest
import function

class TestAdd(unittest.TestCase):
    def test_equal(self):
        result = function.add(2,2)
        self.assertEqual(result, 4)



    def test_false(self):
        result = function.add(2,2)
        self.assertNotEqual(result, 8)

    def test_type(self):
        result = function.add(2,2)
        self.assertEqual(result, int)

if name == 'main':
    unittest.main()